#!/bin/bash

set -e

if [ "$1" = 'help' ]; then

	helpText="js-dev\n\nusage:\ndocker run -it js-dev bash"
	echo -e $helpText

	exit 0
fi

echo -e 'Starting js-dev\n\nVersion check:'
echo 'node'
node -v
echo 'npm'
npm -v
git version

exec "$@"