FROM ubuntu:latest
# latest = LTS

# data dir
RUN mkdir /data

# Replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# update the repository sources list
# and install dependencies
RUN apt-get update \
    && apt-get install -y curl git python2.7 build-essential\
    && apt-get -y autoclean

RUN ln -s /usr/bin/python2.7 /usr/bin/python

# nvm environment variables
ENV NVM_DIR /usr/local/nvm
ENV NVM_VERSION 0.33.8
ENV NODE_VERSION 8.10.0

# install nvm
# https://github.com/creationix/nvm#install-script
RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v$NVM_VERSION/install.sh | bash

# install node and npm
RUN source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

# add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# confirm installation
RUN node -v
RUN npm -v

# entrypoint
COPY ./entrypoint.sh /data
RUN ["chmod", "+x", "/data/entrypoint.sh"]

ENTRYPOINT ["/data/entrypoint.sh"]

CMD ["help"]
