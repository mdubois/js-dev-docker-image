# js-dev-docker-image

Docker image based on Ubuntu and having a few stuff like:
* git
* nvm
* node
* npm

## Build

docker image build -t js-dev .

## Run

docker container run -it js-dev bash
